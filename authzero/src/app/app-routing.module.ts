import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeletubbiesComponent } from './teletubbies/teletubbies.component';
import { LoginComponent } from './login/login.component';
import { CallbackComponent } from './callback/callback.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'teletubbies',
    component: TeletubbiesComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'callback',
    component: CallbackComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }



