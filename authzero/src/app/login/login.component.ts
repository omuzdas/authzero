import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public auth: AuthService, public route:Router) { 
    if(auth.isAuthenticated()) {
      console.log("teletabiye yönlendirme");
      route.navigate(['/teletubbies']);
    }
  }

  ngOnInit() {
  }

}
