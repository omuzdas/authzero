import { Component, OnInit } from '@angular/core';
import { Teletubby } from '../teletubby';
import { AuthService } from '../auth.service';
//import { Route } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teletubbies',
  templateUrl: './teletubbies.component.html',
  styleUrls: ['./teletubbies.component.css']
})
export class TeletubbiesComponent implements OnInit {

  teletubbies: Teletubby[];
  

    
  constructor(public auth: AuthService, public route: Router) {

    
    console.log("Teletubbies");
    if(!auth.isAuthenticated) {
      route.navigate(['/login']);
    }
  }


  ngOnInit() {
    this.teletubbies = [{
      id: 1,
      name: 'Tink Winky',
      description: "Tinky Winky is purple, the biggest Teletubby and almost always goes first!",
      color: "purple",
      imageurl: "tinky-winky.png"
    },
    {
      id: 1,
      name: 'Dipsy',
      description: "Dipsy is green, fun, loves dancing and bouncing on his bottom!",
      color: "green",
      imageurl: "dipsy.png"
    },
    {
      id: 1,
      name: 'Laa laa',
      description: "Laa-Laa is yellow, a performer and she loves to sing and dance!",
      color: "yellow",
      imageurl: "laa-laa.png"
    },
    {
      id: 1,
      name: 'Po',
      description: "Po is red, cute and little! Her favorite thing is going super fast on the scooter!",
      color: "red",
      imageurl: "po.png"
    }
    ];
  }

  


}
